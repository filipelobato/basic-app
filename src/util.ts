import { Injectable } from '@angular/core';

import { ActionSheetController, AlertController, LoadingController } from 'ionic-angular'

import { Storage } from '@ionic/storage';

@Injectable()
export class Util {

  user_id: any;

  baseURL = 'http://localhost/basic-ws/web/';
  //baseURL = 'https://jgtbraga.com/web/';

  loading;

  constructor(public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public storage: Storage) {}

  getStorage(key) {
    return JSON.parse(localStorage.getItem(key))
  }

  setStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data))
  }

  showActionSheet(title, buttons) {
    let actionSheet = this.actionSheetCtrl.create({
      title: title,
      buttons: buttons
   });

   actionSheet.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
  }

  endLoading() {
    this.loading.dismiss();
  }

  showAlert(title, subTitle, button) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [button]
    });

    alert.present();
  }

}
