import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tagSearch',
})
export class TagSearchPipe implements PipeTransform {
  transform(data: any, query: string) {
    if (data == 0)
      return [];

    if (query == null)
      return data;

    return data.filter(data =>
        data.description.toLowerCase().indexOf(query.toLowerCase()) > -1
    );
  }
}
