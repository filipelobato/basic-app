import { Pipe, PipeTransform } from '@angular/core';

import { Util } from '../../util';

@Pipe({
  name: 'productSearch',
})
export class ProductSearchPipe implements PipeTransform {

  dataTag: any = [];
  productTags: any = [];

  constructor(public util: Util) {}

  transform(data: any, query: string) {
    if (data == 0)
      return [];

    if (query == null)
      return data;

    return data.filter(data =>
        data.category.description.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
        data.description.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
        data.quantity.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
        data.price.toLowerCase().indexOf(query.toLowerCase()) > -1
    );
  }

}
