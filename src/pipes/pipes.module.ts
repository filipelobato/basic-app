import { NgModule } from '@angular/core';

import { CategorySearchPipe } from './../pipes/category-search/category-search';
import { TagSearchPipe } from './../pipes/tag-search/tag-search';
import { ProductSearchPipe } from './../pipes/product-search/product-search';

@NgModule({
	declarations: [
		CategorySearchPipe,
    TagSearchPipe,
    ProductSearchPipe
	],
	exports: [
		CategorySearchPipe,
    TagSearchPipe,
    ProductSearchPipe
	]
})
export class PipesModule {}
