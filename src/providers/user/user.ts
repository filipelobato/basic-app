import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Util } from '../../util';

@Injectable()
export class UserProvider {

  constructor(public http: Http, public util: Util) {}

  view() {
    let viewURL = "user/view?id=";
    let url = this.util.baseURL + viewURL + this.util.user_id;

    return this.http.get(url).map(res => res.json());
  }

  update(data) {
    let updateURL = "user/update?id=";
    let url = this.util.baseURL + updateURL + this.util.user_id;

    return this.http.post(url, data).map(res => res.json());
  }

  delete() {

  }

}
