import { NgModule } from '@angular/core';

import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';
import { CategoryProvider } from '../providers/category/category';
import { TagProvider } from '../providers/tag/tag';
import { ProductProvider } from '../providers/product/product';

@NgModule({
	providers: [
		AuthProvider,
		UserProvider,
		CategoryProvider,
		TagProvider,
		ProductProvider,
	]
})
export class ProvidersModule {}
