import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductFormPage } from '../../pages/product-form/product-form';
import { ProductViewPage } from '../../pages/product-view/product-view';
import { ProductProvider } from '../../providers/product/product';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  isList = true;

  dataProduct: any = [];
  query: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public productProvider: ProductProvider,
    public util: Util) {}

  showActionSheet(item) {
    let title = item.description;
    let buttons = [
       {
         text: 'View',
         handler: () => {
           this.view(item);
         }
       },
       {
         text: 'Update',
         handler: () => {
           this.update(item)
         }
       },
       {
         text: 'Delete',
         handler: () => {
           this.delete(item.product_id)
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
       }
     ];
     this.util.showActionSheet(title, buttons);
  }

  view(item) {
    this.navCtrl.push(ProductViewPage, {item: item});
  }

  create() {
    this.navCtrl.push(ProductFormPage, {action: "Create"});
  }

  update(item) {
    this.navCtrl.push(ProductFormPage, {action: "Update", item: item});
  }

  delete(product_id) {
    this.productProvider.delete(product_id).subscribe(
      data => {
        this.productProvider.index().subscribe(data => this.util.setStorage('dataProduct', data));
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
       },
      () => {
        this.navCtrl.push(ProductPage)
       }
    );
  }

  goGrid() {
    this.isList = false;
  }

  goList() {
    this.isList = true;
  }

  getProductTags(item) {
    let tags = item.reduce(function(prevVal, elem) {
      return prevVal + elem.tag.description.replace(/\s+/, '') + ', ';
    }, '')

    return tags.substr(0, tags.length -2);
  }

  doRefresh(refresher) {
    this.productProvider.index().subscribe(
      data => {
        this.util.setStorage('dataProduct', data);
        this.dataProduct = data;
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
       },
      () => {
        setTimeout(() => { refresher.complete() }, 2000)
       }
    )
  }

  searchFn(ev: any) {
    this.query = ev.target.value;
  }

  ionViewDidEnter() {
    this.dataProduct = this.util.getStorage('dataProduct')
  }

}
