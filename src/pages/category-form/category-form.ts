import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoryPage } from '../category/category';
import { CategoryProvider } from '../../providers/category/category';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-category-form',
  templateUrl: 'category-form.html',
})
export class CategoryFormPage {
  action: string;

  dataCategory: any = [];

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public formBuilder: FormBuilder,
    public categoryProvider: CategoryProvider,
    public util: Util) {
    this.action = this.navParams.get("action")
    this.dataCategory = this.navParams.get("item")
    this.initForm()
  }

  doAction() {
    switch(this.action) {
      case "Create":
        this.create()
      break;
      case "Update":
        this.update()
      break;
    }
  }

  create() {
    let description = this.data.value.description;

    let data = JSON.stringify({description: description})

    this.categoryProvider.create(data).subscribe(
      data => {
        this.categoryProvider.index().subscribe(data => this.util.setStorage('dataCategory', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
       },
      () => {
        this.navCtrl.push(CategoryPage)
       }
    )
  }

  update() {
    let description = this.data.value.description;

    let data = JSON.stringify({description: description})

    this.categoryProvider.update(this.dataCategory.category_id, data).subscribe(
      data => {
        this.categoryProvider.index().subscribe(data => this.util.setStorage('dataCategory', data))
       },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
       },
      () => {
        this.navCtrl.push(CategoryPage)
       }
    );
  }

  initForm() {
    this.data = this.formBuilder.group({
      description: [this.action === "Update" ? this.dataCategory.description : "", Validators.required],
    })
  }

}
