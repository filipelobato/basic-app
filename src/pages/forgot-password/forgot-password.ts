import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';

import { AuthProvider } from '../../providers/auth/auth';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public authProvider: AuthProvider) {
    this.initForm()
  }

  initForm() {
    this.data = this.formBuilder.group({
      email: ['', Validators.required],
    })
  }

  forgotPassword() {
    let email = this.data.value.email;

    let data = JSON.stringify({email: email})

    this.authProvider.forgotPassword(data);
  }

  goLogin() {
    this.navCtrl.pop()
  }

  goRegister() {
    this.navCtrl.push(RegisterPage)
  }

}
