import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoryFormPage } from '../../pages/category-form/category-form';
import { CategoryProvider } from '../../providers/category/category';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {

  isList:boolean = true;

  dataCategory: any = [];
  query: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public categoryProvider: CategoryProvider,
    public util: Util) {}

  showActionSheet(item) {
    let title = item.description;
    let buttons = [
       {
         text: 'Update',
         handler: () => {
           this.update(item)
         }
       },
       {
         text: 'Delete',
         handler: () => {
           this.delete(item.category_id)
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
       }
     ];
     this.util.showActionSheet(title, buttons)
  }

  create() {
    this.navCtrl.push(CategoryFormPage, {action: "Create"})
  }

  update(item) {
    this.navCtrl.push(CategoryFormPage, {action: "Update", item: item})
  }

  delete(category_id) {
    this.categoryProvider.delete(category_id).subscribe(
      data => {
        this.categoryProvider.index().subscribe(data => this.util.setStorage('dataCategory', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(CategoryPage)
      }
    )
  }

  goList() {
    this.isList = true;
  }

  goGrid() {
    this.isList = false;
  }

  doRefresh(refresher) {
    this.categoryProvider.index().subscribe(
      data => {
        this.util.setStorage('dataCategory', data)
        this.dataCategory = data
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        setTimeout(() => { refresher.complete() }, 2000)
      }
    )
  }

  searchFn(ev) {
    this.query = ev.target.value;
  }

  ionViewDidEnter() {
    this.dataCategory = this.util.getStorage('dataCategory')
  }

}
