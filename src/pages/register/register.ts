import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { AuthProvider } from '../../providers/auth/auth';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public util: Util, public authProvider: AuthProvider) {
    this.initForm();
  }

  initForm() {
    this.data = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      location: ['', Validators.required],
      birthday_date: ['', Validators.required],
      sex: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  register() {
    let username = this.data.value.username;
    let email = this.data.value.email;
    let password = this.data.value.password;

    let data = JSON.stringify({username: username, email: email, password: password});

    this.authProvider.register(data).subscribe(
      data => {
        this.util.showLoading();
        this.util.endLoading()
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(LoginPage)
       }
    );
  }

  goLogin() {
    this.navCtrl.pop();
  }

}
