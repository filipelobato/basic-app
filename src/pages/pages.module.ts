import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { UserPage } from '../pages/user/user';
import { UserFormPage } from '../pages/user-form/user-form';
import { CategoryPage } from '../pages/category/category';
import { CategoryFormPage } from '../pages/category-form/category-form';
import { TagPage } from '../pages/tag/tag';
import { TagFormPage } from '../pages/tag-form/tag-form';
import { ProductPage } from '../pages/product/product';
import { ProductFormPage } from '../pages/product-form/product-form';
import { ProductViewPage } from '../pages/product-view/product-view';

import { PipesModule } from '../pipes/pipes.module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule,
		PipesModule
	],
	declarations: [
		TabsPage,
    LoginPage,
    RegisterPage,
    ForgotPasswordPage,
    UserPage,
    UserFormPage,
    CategoryPage,
    CategoryFormPage,
    TagPage,
    TagFormPage,
    ProductPage,
    ProductFormPage,
    ProductViewPage
  ],
	entryComponents: [
		TabsPage,
		LoginPage,
		RegisterPage,
		ForgotPasswordPage,
		UserPage,
		UserFormPage,
		CategoryPage,
		CategoryFormPage,
		TagPage,
		TagFormPage,
		ProductPage,
		ProductFormPage,
		ProductViewPage
  ],
})
export class PagesModule {}
