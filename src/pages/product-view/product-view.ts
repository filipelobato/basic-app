import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-product-view',
  templateUrl: 'product-view.html',
})
export class ProductViewPage {

  dataProduct: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public util: Util) {
  	this.dataProduct = this.navParams.get('item');
  }

  getProductTags() {
    let tags = this.dataProduct.productTags.reduce(function(prevVal, elem) {
      return prevVal + elem.tag.description.replace(/\s+/, '') + ', ';
    }, '')

    return tags.substr(0, tags.length -2);
  }

}
