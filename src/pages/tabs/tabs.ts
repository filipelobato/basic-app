import { Component } from '@angular/core';

import { UserPage } from '../user/user';
import { CategoryPage } from '../category/category';
import { TagPage } from '../tag/tag';
import { ProductPage } from '../product/product';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tabUser = UserPage;
  tabCategory = CategoryPage;
  tabTag = TagPage;
  tabProduct = ProductPage;

  constructor() {}

}
