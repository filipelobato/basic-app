import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { RegisterPage } from '../register/register';

import { AuthProvider } from '../../providers/auth/auth';
import { CategoryProvider } from '../../providers/category/category';
import { TagProvider } from '../../providers/tag/tag';
import { ProductProvider } from '../../providers/product/product';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  dataUser: any;

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public authProvider: AuthProvider,
    public categoryProvider: CategoryProvider,
    public tagProvider: TagProvider,
    public productProvider: ProductProvider,
    public util: Util) {
    this.initForm()
  }

  initForm() {
    this.data = this.formBuilder.group({
      email: ['succ@gmail.com', Validators.required],
      password: ['123123', Validators.required]
    });
  }

  login() {
    let email = this.data.value.email;
    let password = this.data.value.password;

    let data = JSON.stringify({email: email, password: password})

    this.authProvider.login(data).subscribe(
      data => {
        if (data != '0') {
          localStorage.setItem('isLogged', 'true')
          this.util.user_id = data.user_id;
          this.util.setStorage('dataUser', data)
          this.categoryProvider.index().subscribe(data => this.util.setStorage('dataCategory', data))
          this.tagProvider.index().subscribe(data => this.util.setStorage('dataTag', data))
          this.productProvider.index().subscribe(data => this.util.setStorage('dataProduct', data))
        } else {
          this.util.showAlert('Attention', 'Incorrect e-mail or password', 'Try Again')
        }
       },
      err => { this.util.showAlert('Attention', 'Server Error', 'Try Again') },
      () => { this.navCtrl.push(TabsPage) }
    );
  }

  goForgotPassword() {
    this.navCtrl.push(ForgotPasswordPage)
  }

  goRegister() {
    this.navCtrl.push(RegisterPage)
  }

}
