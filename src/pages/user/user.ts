import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserFormPage } from '../../pages/user-form/user-form';
import { LoginPage } from '../../pages/login/login';

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import { CategoryProvider } from '../../providers/category/category';
import { TagProvider } from '../../providers/tag/tag';
import { ProductProvider } from '../../providers/product/product';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  dataUser: any = [];

  countCategory;
  countTag;
  countProduct;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public util: Util,
    public authProvider: AuthProvider,
    public userProvider: UserProvider,
    public categoryProvider: CategoryProvider,
    public tagProvider: TagProvider,
    public productProvider: ProductProvider) {}

  update() {
    this.navCtrl.push(UserFormPage, {dataUser: this.dataUser});
  }

  doRefresh(refresher) {
    this.userProvider.view().subscribe(
      data => {
         this.util.setStorage('dataUser', data)
         this.dataUser = data;
         this.categoryProvider.index().subscribe(data => { this.countCategory = data.length; })
         this.tagProvider.index().subscribe(data => { this.countTag = data.length; })
         this.productProvider.index().subscribe(data => { this.countProduct = data.length; })
       },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        setTimeout(() => { refresher.complete(); }, 2000)
      }
    )
  }

  camera() {}

  logout() {
    localStorage.setItem('isLogged', 'false')
    this.navCtrl.parent.parent.setRoot(LoginPage);
  }

  ionViewDidEnter() {
    this.dataUser = this.util.getStorage('dataUser')
    this.countCategory = this.util.getStorage('dataCategory').length;
    this.countTag = this.util.getStorage('dataTag').length;
    this.countProduct = this.util.getStorage('dataProduct').length;
  }

}
