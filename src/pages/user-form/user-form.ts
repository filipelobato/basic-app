import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserPage } from '../../pages/user/user';

import { UserProvider } from '../../providers/user/user';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-user-form',
  templateUrl: 'user-form.html',
})
export class UserFormPage {

  dataUser: any;

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public userProvider: UserProvider,
    public util: Util) {
    this.dataUser = this.navParams.get('dataUser');
    this.initForm();
  }

  update() {
    let username = this.data.value.username;
    let email = this.data.value.email;

    let data = JSON.stringify({username: username, email: email});

    this.userProvider.update(data).subscribe(
      data => {
        this.userProvider.view().subscribe(data => this.util.setStorage('datauser', data));
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again');
      },
      () => {
        this.navCtrl.push(UserPage);
      }
    );
  }

  initForm() {
    this.data = this.formBuilder.group({
      username: [this.dataUser.username, Validators.required],
      email: [this.dataUser.email, Validators.required],
      birthday_date: ["1993-03-30", Validators.required],
      sex: ["Male", Validators.required],
      locate: ["USA / California", Validators.required],
    });
  }

}
