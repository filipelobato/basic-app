import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TagFormPage } from '../../pages/tag-form/tag-form';

import { TagProvider } from '../../providers/tag/tag';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-tag',
  templateUrl: 'tag.html',
})
export class TagPage {

  isList = true;

  dataTag: any = [];
  query: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public tagProvider: TagProvider,
    public util: Util) {}

  showActionSheet(item) {
    let title = item.description;
    let buttons = [
       {
         text: 'Update',
         handler: () => {
           this.update(item)
         }
       },
       {
         text: 'Delete',
         handler: () => {
           this.delete(item.tag_id)
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
       }
     ];
     this.util.showActionSheet(title, buttons);
  }

  create() {
    this.navCtrl.push(TagFormPage, {action: "Create"});
  }

  update(item) {
    this.navCtrl.push(TagFormPage, {action: "Update", item: item});
  }

  delete(tag_id) {
    this.tagProvider.delete(tag_id).subscribe(
      data => {
        this.tagProvider.index().subscribe(data => this.util.setStorage('dataTag', data));
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again');
      },
      () => {
        this.navCtrl.push(TagPage);
      }
    );
  }

  goGrid() {
    this.isList = false;
  }

  goList() {
    this.isList = true;
  }

  doRefresh(refresher) {
    this.tagProvider.index().subscribe(
      data => {
        this.util.setStorage('dataTag', data)
        this.dataTag = data;
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        setTimeout(() => { refresher.complete(); }, 2000)
      }
    )
  }

  searchFn(ev: any) {
    this.query = ev.target.value;
  }

  ionViewDidEnter() {
    this.dataTag = this.util.getStorage('dataTag')
  }

}
