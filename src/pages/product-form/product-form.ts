import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProductPage } from '../product/product';
import { ProductProvider } from '../../providers/product/product';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-product-form',
  templateUrl: 'product-form.html',
})
export class ProductFormPage {

  action: string;

  dataProduct: any = [];
  productTags: any = [];

  dataCategory: any = [];
  dataTag: any = [];

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public util: Util,
    public http: Http,
    public formBuilder: FormBuilder,
    public productProvider: ProductProvider) {
    this.action = this.navParams.get("action")
    this.dataProduct = this.navParams.get("item")
    this.initForm()
    if (this.action === "Update")
      this.setProductTags()
  }

  doAction() {
    switch(this.action) {
      case "Create":
        this.create()
      break;
      case "Update":
        this.update()
      break;
    }
  }

  create() {
    let category_id = this.data.value.category_id;
    let productTags = this.data.value.productTags;
    let description = this.data.value.description;
    let quantity = this.data.value.quantity;
    let price = this.data.value.price;

    let data = JSON.stringify({category_id: category_id, productTags: productTags, description: description, quantity: quantity, price: price, img: "3.png"})

    this.productProvider.create(data).subscribe(
      data => {
        this.productProvider.index().subscribe(data => this.util.setStorage('dataProduct', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(ProductPage)
      }
    );
  }

  update() {
    let category_id = this.data.value.category_id;
    let productTags = this.data.value.productTags;
    let description = this.data.value.description;
    let quantity = this.data.value.quantity;
    let price = this.data.value.price;

    let data = JSON.stringify({category_id: category_id, productTags: productTags, description: description, quantity: quantity, price: price, img: "3.png"})

    this.productProvider.update(this.dataProduct.product_id, data).subscribe(
      data => {
        this.productProvider.index().subscribe(data => this.util.setStorage('dataProduct', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(ProductPage);
      }
    );
  }

  camera() {

  }

  initForm() {
    this.data = this.formBuilder.group({
      category_id: [this.action === "Update" ? this.dataProduct.category_id : "", Validators.required],
      productTags: [this.action === "Update" ? this.productTags : "", Validators.required],
      description: [this.action === "Update" ? this.dataProduct.description : "", Validators.required],
      quantity: [this.action === "Update" ? this.dataProduct.quantity : ""],
      price: [this.action === "Update" ? this.dataProduct.price : ""],
    });
  }

  setProductTags() {
    this.dataProduct.productTags.map(obj => this.productTags.push(obj.tag_id));
  }

  ionViewDidEnter() {
    this.dataCategory = this.util.getStorage('dataCategory')
    this.dataTag = this.util.getStorage('dataTag')
  }

}
