import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TagFormPage } from './tag-form';

@NgModule({
  declarations: [
    TagFormPage,
  ],
  imports: [
    IonicPageModule.forChild(TagFormPage),
  ],
})
export class TagFormPageModule {}
