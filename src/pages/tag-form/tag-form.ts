import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TagPage } from '../tag/tag';

import { TagProvider } from '../../providers/tag/tag';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Util } from '../../util';

@IonicPage()
@Component({
  selector: 'page-tag-form',
  templateUrl: 'tag-form.html',
})
export class TagFormPage {

  action: string;

  dataTag: any = [];

  private data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public util: Util,
    public http: Http,
    public formBuilder: FormBuilder,
    public tagProvider: TagProvider) {
    this.action = this.navParams.get("action");
    this.dataTag = this.navParams.get("item");
    this.initForm();
  }

  doAction() {
    switch(this.action) {
      case "Create":
        this.create();
      break;
      case "Update":
        this.update();
      break;
    }
  }

  create() {
    let description = this.data.value.description;

    let data = JSON.stringify({description: description});

    this.tagProvider.create(data).subscribe(
      data => {
        this.tagProvider.index().subscribe(data => this.util.setStorage('dataTag', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(TagPage)
      }
    );
  }

  update() {
    let description = this.data.value.description;

    let data = JSON.stringify({description: description});

    this.tagProvider.update(this.dataTag.tag_id, data).subscribe(
      data => {
        this.tagProvider.index().subscribe(data => this.util.setStorage('dataTag', data))
      },
      err => {
        this.util.showAlert('Attention', 'Server Error', 'Try Again')
      },
      () => {
        this.navCtrl.push(TagPage)
      }
    );
  }

  initForm() {
    this.data = this.formBuilder.group({
      description: [this.action === "Update" ? this.dataTag.description : "", Validators.required],
    });
  }

}
